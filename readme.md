# Quiz

Command line quizzing script for Unix-based and Unix-like operating systems.

## Prerequisites

* Coreutils
* BASH

## Usage

1. Download the script: `git clone https://gitlab.com/TackyMan/quiz`
2. Move into the main directory: `cd quiz/`
3. Make quiz.sh executable: `chmod 777 quiz.sh`
4. Create a TSV (Tab-separated values) file with two columns.
5. Execute the script with the TSV file name as an argument: `./quiz.sh path-to-tsv`
6. Hit the number key of the answer you want to choose.
7. Answer all the questions correctly, or hit 'q' to quit.
