#!/bin/bash

clear

LINES=$(wc -l $1 | cut -d ' ' -f1)

cat $1 | sed -e 's/$/\t0/' | shuf > $1.tmp

while [ $LINES -ge 1 ]
do
	LINENUMBER=$(( ( RANDOM % $LINES ) + 1))

	echo
	echo ${LINES} questions remaining.
	echo
	sed "${LINENUMBER}q;d" $1.tmp | cut -f1
	echo
	shuf $1 | cat <(sed "4q") <(sed "${LINENUMBER}q;d" $1.tmp) | cut -f 2 | tail -n 4 | shuf | cat -n > $1.tmp.tmp

	cat $1.tmp.tmp

	read -n 1 ANSWER

    if [ $ANSWER == 'q' ]
    then
        break
	elif [ "$(sed "${ANSWER}q;d" $1.tmp.tmp | cut -f2)" == "$(sed "${LINENUMBER}q;d" $1.tmp | cut -f2)" ]
	then
		sed "${LINENUMBER}d" -i $1.tmp
		((LINES--))
	fi

	clear
done

rm $1.tmp $1.tmp.tmp

clear
